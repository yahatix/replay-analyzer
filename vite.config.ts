import { sveltekit } from "@sveltejs/kit/vite"
import type { UserConfig } from "vite"
import wasmPack from "vite-plugin-wasm-pack"

const config: UserConfig = {
  plugins: [sveltekit(), wasmPack("./wasm-replay-reader")]
}

export default config