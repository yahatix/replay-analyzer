/* tslint:disable */
/* eslint-disable */
export const memory: WebAssembly.Memory;
export function parse(a: number, b: number): void;
export function __wbindgen_add_to_stack_pointer(a: number): number;
export function __wbindgen_export_0(a: number, b: number): void;
export function __wbindgen_export_1(a: number): number;
export function __wbindgen_export_2(a: number, b: number, c: number): number;
