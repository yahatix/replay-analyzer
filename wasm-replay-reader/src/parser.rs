use crate::reader::{Reader, Vec3d, Vec4d};
use serde::Serialize;

#[derive(Serialize, Debug)]
pub struct Parser {
    #[serde(skip_serializing)]
    pub reader: Reader,
    pub meta: Option<Meta>,
    pub header: Option<Header>,
    pub match_stats: Option<MatchStats>,
    pub team_match_stats: Option<TeamMatchStats>,
    pub eliminations: Vec<Elimination>,
}

#[derive(Serialize, Debug)]
pub struct ParserResult {
    pub meta: Option<Meta>,
    pub header: Option<Header>,
    pub match_stats: Option<MatchStats>,
    pub team_match_stats: Option<TeamMatchStats>,
    pub eliminations: Vec<Elimination>,
}

#[derive(Serialize, Debug)]
pub struct Meta {
    pub magic: u32,
    pub file_version: u32,
    pub length_in_ms: u32,
    pub network_version: u32,
    pub changelist: u32,
    pub name: String,
    pub is_live: bool,
    pub timestamp: Option<u32>,
    pub is_compressed: bool,
    pub is_encrypted: bool,
}

#[derive(Serialize, Debug)]
pub struct GameVersion {
    pub branch: String,
    pub patch: u16,
    pub changelist: u32,
    pub major: u32,
    pub minor: u32,
}

#[derive(Serialize, Debug)]
pub struct Header {
    pub magic: u32,
    pub network_version: u32,
    pub network_checksum: u32,
    pub engine_network_version: u32,
    pub game_network_protocol: u32,
    pub id: Option<String>,
    pub version: GameVersion,
    pub level_names_and_times: Vec<(String, u32)>,
    pub flags: u32,
    pub game_specific_data: Vec<String>,
    pub platform: String,
    pub file_version_ue4: u32,
    pub file_version_ue5: u32,
    pub package_version_licensee_ue: u32,
}

#[derive(Serialize, Debug)]
pub struct Player {
    pub id: String,
    pub name: String,
    pub is_bot: bool,
    pub rotation: Vec4d,
    pub location: Vec3d,
    pub scale: Vec3d,
}

impl Player {
    pub fn new() -> Player {
        Player {
            id: "".to_string(),
            name: "".to_string(),
            is_bot: false,
            rotation: Vec4d {
                x: 0.0,
                y: 0.0,
                z: 0.0,
                w: 0.0,
            },
            location: Vec3d {
                x: 0.0,
                y: 0.0,
                z: 0.0,
            },
            scale: Vec3d {
                x: 0.0,
                y: 0.0,
                z: 0.0,
            },
        }
    }
}

#[derive(Serialize, Debug)]
pub struct Elimination {
    pub eliminated: Player,
    pub eliminator: Player,
    pub gun_type: String,
    pub knocked: bool,
    pub timestamp: u32,
    pub offset: usize,
}

#[derive(Serialize, Debug)]
pub struct TeamMatchStats {
    pub position: u32,
    pub total_players: u32,
}

#[derive(Serialize, Debug)]
pub struct MatchStats {
    pub accuracy: f32,
    pub assists: u32,
    pub eliminations: u32,
    pub weapon_damage: u32,
    pub other_damage: u32,
    pub revives: u32,
    pub damage_taken: u32,
    pub damage_to_structures: u32,
    pub materials_gathered: u32,
    pub materials_used: u32,
    pub total_traveled: u32,
}

impl Parser {
    pub fn new(path: Vec<u8>) -> Self {
        let reader = Reader::new(path.to_vec());

        Self {
            reader,
            meta: None,
            header: None,
            match_stats: None,
            team_match_stats: None,
            eliminations: vec![],
        }
    }

    pub fn parse(&mut self) {
        self.parse_meta();
        self.parse_chunks();
    }

    pub fn parse_meta(&mut self) {
        let magic = self.reader.read_u32();
        let file_version = self.reader.read_u32();
        let length_in_ms = self.reader.read_u32();
        let network_version = self.reader.read_u32();
        let changelist = self.reader.read_u32();
        let name = String::from(self.reader.read_string().trim_end());
        let is_live = self.reader.read_bool();

        let mut timestamp = None;
        if file_version >= 3 {
            timestamp = Some(((self.reader.read_u64() - 621355968000000000) / 100000) as u32);
        }

        let mut is_compressed = false;
        if file_version >= 2 {
            is_compressed = self.reader.read_bool();
        }

        let mut is_encrypted = false;
        if file_version >= 6 {
            is_encrypted = self.reader.read_bool();
            let key_length = self.reader.read_u32();
            self.reader.encryption_key =
                Some(self.reader.read_bytes(&(key_length as usize)).to_vec());
        }

        self.meta = Some(Meta {
            magic,
            file_version,
            length_in_ms,
            network_version,
            changelist,
            name,
            is_live,
            timestamp,
            is_compressed,
            is_encrypted,
        });
    }

    pub fn parse_chunks(&mut self) {
        while self.reader.buffer.len() > self.reader.offset {
            let chunk_type = self.reader.read_u32();
            let chunk_size = self.reader.read_i32();
            let start_offset = self.reader.offset;

            match chunk_type {
                0 => {
                    self.header = Some(self.parse_header());
                }
                1 => { /* Replay Data */ }
                2 => { /* Checkpoint */ }
                3 => {
                    self.parse_event();
                }
                _ => {}
            }

            self.reader.offset = start_offset + chunk_size as usize;
        }
    }

    pub fn parse_header(&mut self) -> Header {
        let mut result: Header = Header {
            magic: 0,
            network_version: 0,
            network_checksum: 0,
            engine_network_version: 0,
            game_network_protocol: 0,
            id: Some(String::from("")),
            version: GameVersion {
                branch: String::from(""),
                patch: 0,
                changelist: 0,
                major: 0,
                minor: 0,
            },
            file_version_ue4: 0,
            file_version_ue5: 0,
            package_version_licensee_ue: 0,
            level_names_and_times: Vec::from([]),
            flags: 0,
            game_specific_data: Vec::from([]),
            platform: String::from(""),
        };

        result.magic = self.reader.read_u32();
        self.reader.offset = 744;
        result.network_version = self.reader.read_u32();

        result.network_checksum = self.reader.read_u32();
        result.engine_network_version = self.reader.read_u32();
        result.game_network_protocol = self.reader.read_u32();

        if result.network_version > 12 {
            result.id = Some(self.reader.read_id());
        }

        self.reader.skip(&4);
        result.version.patch = self.reader.read_u16();
        result.version.changelist = self.reader.read_u32();
        result.version.branch = self.reader.read_string();

        let mut iter = result
            .version
            .branch
            .trim_start_matches("++Fortnite+Release-")
            .split('.');

        result.version.major = iter.next().unwrap().parse().unwrap();
        result.version.minor = iter.next().unwrap().parse().unwrap();

        if result.network_version >= 18 {
            result.file_version_ue4 = self.reader.read_u32();
            result.file_version_ue5 = self.reader.read_u32();
            result.package_version_licensee_ue = self.reader.read_u32();
        }

        result.level_names_and_times = self.reader.read_string_u32_tuple_vec();
        result.flags = self.reader.read_u32();
        result.game_specific_data = self.reader.read_string_vec();

        result
    }

    pub fn parse_event(&mut self) {
        let _id = self.reader.read_string();
        let group = self.reader.read_string();
        let metadata = self.reader.read_string();
        let start_time = self.reader.read_u32();
        let _end_time = self.reader.read_u32();
        let length = self.reader.read_u32();

        let encrypted_buffer = self.reader.read_bytes(&(length as usize)).to_vec();
        let mut buffer_reader = self.reader.decrypt_buffer(encrypted_buffer);

        if group == "playerElim" {
            self.parse_elimination(&mut buffer_reader, start_time);
        } else if metadata == "AthenaMatchStats" {
            self.match_stats = Some(self.parse_match_stats(&mut buffer_reader));
        } else if metadata == "AthenaMatchTeamStats" {
            self.team_match_stats = Some(self.parse_team_match_stats(&mut buffer_reader));
        } else if metadata == "PlayerStateEncryptionKey" {
            // ignore
        }
    }

    pub fn parse_elimination(&mut self, data: &mut Reader, timestamp: u32) {
        let header = self.header.as_ref().expect("Header was not found");
        let offset = self.reader.offset;

        let mut eliminated = Player::new();
        let mut eliminator = Player::new();

        log::info!("engine_network_version: {}", header.engine_network_version);

        if header.engine_network_version >= 11 && header.version.major >= 9 {
            if header.engine_network_version >= 23 {
                // eliminated.rotation = data.read_vec_4d();
                // eliminated.location = data.read_vec_3d();
                // eliminated.scale = data.read_vec_3d();

                // data.skip(&5);

                // eliminator.rotation = data.read_vec_4d();
                // eliminator.location = data.read_vec_3d();
                // eliminator.scale = data.read_vec_3d();

                data.skip(&165);
            } else {
                data.skip(&85)
            }
            eliminated.id = self.parse_player(data);
            eliminator.id = self.parse_player(data);
        } else {
            if header.version.major <= 4 && header.version.minor < 2 {
                self.reader.skip(&12)
            } else if header.version.major == 4 && header.version.minor <= 2 {
                self.reader.skip(&40)
            } else {
                self.reader.skip(&45)
            }
            eliminated.name = data.read_string();
            eliminator.name = data.read_string();
        }

        let gun_type_byte = data.read_byte();

        let gun_type = match gun_type_byte {
            0 => "None",
            1 => "RangedAny",
            2 => "Pistol",
            3 => "Shotgun",
            4 => "Rifle",
            5 => "SMG",
            6 => "Sniper",
            7 => "GrenadeLauncher",
            8 => "RocketLauncher",
            9 => "Bow",
            10 => "Minigun",
            11 => "LMG",
            12 => "BiplaneGun",
            13 => "MeleeAny",
            14 => "Harvesting",
            15 => "MAX",
            _ => "Unknown",
        }
        .to_string();

        let knocked = data.read_bool();

        self.eliminations.push(Elimination {
            eliminated,
            eliminator,
            gun_type,
            knocked,
            timestamp,
            offset,
        });
    }

    pub fn parse_player(&mut self, data: &mut Reader) -> String {
        let player_type = data.read_byte();

        match player_type {
            3 => "Bot".to_string(),
            16 => data.read_string(),
            17 => {
                data.skip(&1);
                data.read_id()
            }
            _ => "Invalid".to_string(),
        }
    }

    pub fn parse_team_match_stats(&mut self, data: &mut Reader) -> TeamMatchStats {
        TeamMatchStats {
            position: data.read_u32(),
            total_players: data.read_u32(),
        }
    }

    pub fn parse_match_stats(&mut self, data: &mut Reader) -> MatchStats {
        MatchStats {
            accuracy: data.read_f32(),
            assists: data.read_u32(),
            eliminations: data.read_u32(),
            weapon_damage: data.read_u32(),
            other_damage: data.read_u32(),
            revives: data.read_u32(),
            damage_taken: data.read_u32(),
            damage_to_structures: data.read_u32(),
            materials_gathered: data.read_u32(),
            materials_used: data.read_u32(),
            total_traveled: data.read_u32(),
        }
    }
}
