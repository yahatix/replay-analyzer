mod parser;
mod reader;
use crate::parser::Parser;
use std::fs;
pub fn main() {
    for file in fs::read_dir("./replayFiles").unwrap() {
        let file = file.expect("Failed to get entry");
        if file.file_type().unwrap().is_dir() || !file.file_name().into_string().unwrap().contains(".replay") {
            continue;
        }

        let replay_file = file.file_name().to_string_lossy().to_string();
        let file_contents = fs::read(format!("./replayFiles/{}",replay_file)).unwrap();
        let mut psr = Parser::new(file_contents);
        psr.parse();
        let output_file_path = format!("./replayFiles/output/{}.json", replay_file);
        fs::write(output_file_path, serde_json::to_string_pretty(&psr).unwrap()).expect("TODO: panic message");
    }
}

#[cfg(test)]
mod tests {
    use crate::parser::Parser;
    use std::fs;

    #[test]
    fn test3() {
        for file in fs::read_dir("./replayFiles").unwrap() {
            let file = file.expect("Failed to get entry");
            if file.file_type().unwrap().is_dir() || !file.file_name().into_string().unwrap().contains(".replay") {
                continue;
            }

            let replay_file = file.file_name().to_string_lossy().to_string();
            println!("Parsing replay file: {}", replay_file);
            let file_contents = fs::read(format!("./replayFiles/{}",replay_file)).unwrap();
            let mut psr = Parser::new(file_contents);
            psr.parse();
            let output_file_path = format!("./replayFiles/output/{}.json", replay_file);
            fs::write(output_file_path, serde_json::to_string_pretty(&psr).unwrap()).expect("TODO: panic message");
        }
    }
}
