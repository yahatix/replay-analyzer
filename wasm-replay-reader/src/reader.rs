use aes_soft::Aes256;
use block_modes::{block_padding::ZeroPadding, BlockMode, Ecb};
use byteorder::{ByteOrder, LittleEndian};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct Reader {
    pub buffer: Vec<u8>,
    pub offset: usize,
    pub encryption_key: Option<Vec<u8>>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Vec3d {
    pub x: f64,
    pub y: f64,
    pub z: f64,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Vec4d {
    pub x: f64,
    pub y: f64,
    pub z: f64,
    pub w: f64,
}

impl Reader {
    pub fn new(buffer: Vec<u8>) -> Self {
        Self {
            buffer,
            offset: 0,
            encryption_key: None,
        }
    }

    pub fn skip(&mut self, byte_count: &usize) {
        self.offset += *byte_count;
    }

    pub fn read_u16(&mut self) -> u16 {
        let num = LittleEndian::read_u16(&self.buffer[self.offset..self.offset + 2]);
        self.skip(&2);
        num
    }

    pub fn read_u32(&mut self) -> u32 {
        let num = LittleEndian::read_u32(&self.buffer[self.offset..self.offset + 4]);
        self.skip(&4);
        num
    }

    pub fn read_u64(&mut self) -> u64 {
        let num = LittleEndian::read_u64(&self.buffer[self.offset..self.offset + 8]);
        self.skip(&8);
        num
    }

    pub fn read_i32(&mut self) -> i32 {
        let num = LittleEndian::read_i32(&self.buffer[self.offset..self.offset + 4]);
        self.skip(&4);
        num
    }

    pub fn read_f32(&mut self) -> f32 {
        let num = LittleEndian::read_f32(&self.buffer[self.offset..self.offset + 4]);
        self.skip(&4);
        num
    }

    pub fn read_f64(&mut self) -> f64 {
        let num = LittleEndian::read_f64(&self.buffer[self.offset..self.offset + 8]);
        self.skip(&8);
        num
    }

    pub fn read_byte(&mut self) -> u8 {
        let byte = self.buffer[self.offset..self.offset + 1][0];
        self.skip(&1);
        byte
    }

    pub fn read_bytes(&mut self, &byte_count: &usize) -> &[u8] {
        let bytes = &self.buffer[self.offset..self.offset + byte_count];
        self.offset += byte_count;
        bytes
    }

    pub fn read_bool(&mut self) -> bool {
        self.read_i32() == 1
    }

    pub fn read_id(&mut self) -> String {
        let bytes = self.read_bytes(&16);
        let mut id = String::from("");

        for byte in bytes.iter() {
            id.push_str(&format!("{:02X}", byte));
        }

        id.to_lowercase()
    }

    pub fn read_string(&mut self) -> String {
        let string_length = self.read_i32();
        match string_length {
            0 => String::from(""),
            n if n < 0 => {
                let mut u16_vec: Vec<u16> = vec![];

                for _ in 0..-string_length {
                    u16_vec.push(self.read_u16());
                }
                u16_vec.pop();

                String::from_utf16(&u16_vec).expect("Cannot parse u16 vector to utf16 string")
            }
            _ => {
                let bytes = self.read_bytes(&(string_length as usize));
                let mut byte_vec: Vec<u8> = bytes.to_vec();
                byte_vec.pop();

                String::from_utf8(byte_vec).expect("Cannot parse u8 vector to utf8 string")
            }
        }
    }

    pub fn read_string_vec(&mut self) -> Vec<String> {
        let array_length = self.read_u32();
        let mut vec: Vec<String> = vec![];

        for _ in 0..array_length {
            vec.push(self.read_string())
        }

        vec
    }

    pub fn read_string_u32_tuple_vec(&mut self) -> Vec<(String, u32)> {
        let array_length = self.read_u32();
        let mut vec: Vec<(String, u32)> = vec![];

        for _ in 0..array_length {
            vec.push((self.read_string(), self.read_u32()));
        }

        vec
    }

    pub fn decrypt_buffer(&mut self, data: Vec<u8>) -> Self {
        let raw_key = match &self.encryption_key {
            Some(key) => key,
            None => panic!("No encryption key found"),
        };

        let mut encrypted_data = data;

        let decrypt = Ecb::<Aes256, ZeroPadding>::new_var(raw_key, Default::default()).unwrap();
        let decrypted_data = decrypt.decrypt(&mut encrypted_data).unwrap();

        Self {
            offset: 0,
            buffer: decrypted_data.to_vec(),
            encryption_key: None,
        }
    }
    
    #[allow(dead_code)]
    pub fn read_vec_3d(&mut self) -> Vec3d {
        Vec3d {
            x: self.read_f64(),
            y: self.read_f64(),
            z: self.read_f64(),
        }
    }
    
    #[allow(dead_code)]
    pub fn read_vec_4d(&mut self) -> Vec4d {
        Vec4d {
            x: self.read_f64(),
            y: self.read_f64(),
            z: self.read_f64(),
            w: self.read_f64(),
        }
    }
}
