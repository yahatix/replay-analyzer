mod parser;
mod reader;

extern crate console_error_panic_hook;
use js_sys::Uint8Array;
use std::panic;
use wasm_bindgen::prelude::*;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[allow(non_upper_case_globals, non_camel_case_types, non_snake_case)]
#[wasm_bindgen]
pub fn parse(arr: &Uint8Array) -> Result<JsValue, JsValue> {
    panic::set_hook(Box::new(console_error_panic_hook::hook));
    console_log::init().expect("logger could not be initialized");

    let buffer = arr.to_vec();
    let mut psr = parser::Parser::new(buffer);
    psr.parse();

    Ok(serde_wasm_bindgen::to_value(&psr)?)
}
