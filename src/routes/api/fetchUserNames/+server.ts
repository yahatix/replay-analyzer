import { queryAccounts } from '$lib/Http'
import type { RequestHandler } from "@sveltejs/kit"

export const POST: RequestHandler = async ({ request }) => {
    const data = await request.json()

    const accounts = (await queryAccounts(data)).flat(1)

    console.log(accounts);
    

    return new Response(JSON.stringify({ accounts }))
}