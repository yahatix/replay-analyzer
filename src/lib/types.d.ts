 export interface ReplayEncryption {
    is_encrypted?: boolean;
    encryption_key?: Buffer;
}
export interface ReplayMeta {
    magic: number;
    file_version: number;
    length_in_ms: number;
    network_version: number;
    changelist: number;
    name: string;
    is_live: boolean;
    timestamp?: Date;
    is_compressed?: boolean;
}
export interface ReplayHeader {
    magic: number;
    network_version: number;
    network_checksum: number;
    engine_network_version: number;
    game_network_protocol: number;
    id: string | undefined;
    version: {
        branch: string;
        major: number;
        minor: number;
        changelist: number;
        patch: number;
    };
    level_names_and_times: any[];
    flags: number;
    game_specific_data: any[];
}
export interface ReplayPlayer {
    name?: string;
    id?: string;
    is_bot: boolean;
}
export interface ReplayElimination {
    eliminated: ReplayPlayer;
    eliminator: ReplayPlayer;
    gun_type: string;
    knocked: boolean;
    timestamp: number;
}
export interface ReplayMatchStats {
    accuracy: number;
    assists: number;
    eliminations: number;
    weapon_damage: number;
    other_damage: number;
    revives: number;
    damage_taken: number;
    damage_to_structures: number;
    materials_gathered: number;
    materials_used: number;
    total_traveled: number;
}
export interface ReplayTeamMatchStats {
    position: number;
    total_players: number;
}
export interface ReplayFile {
    meta: ReplayMeta;
    header: ReplayHeader;
    match_stats: ReplayMatchStats | undefined;
    team_match_stats: ReplayTeamMatchStats | undefined;
    eliminations: ReplayElimination[];
    player_names?: string[]
  }