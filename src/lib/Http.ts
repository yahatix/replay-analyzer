const accountQuery = `
query AccountQuery($accountIds: [String]!) {
Account {
    accounts(accountIds: $accountIds) {
        id
        displayName
        externalAuths {
            externalDisplayName
            }
        }
    }
}
`;

export type UserInfo = {
    id: string
    displayName: string
    externalAuths: {
        externalDisplayName: string
    }
}

const queryAccounts = async (accountIds: string[]): Promise<UserInfo[]> => {
    const chunkedAccounts = accountIds.reduce((resArr: any[], id, i) => {
        const chunkIndex = Math.floor(i / 100);
        // eslint-disable-next-line no-param-reassign
        if (!resArr[chunkIndex]) resArr[chunkIndex] = [];
        resArr[chunkIndex].push(id);
        return resArr;
    }, []);

    const accounts = await Promise.all(chunkedAccounts.map((accountChunk) => {
        return fetch('https://graphql.epicgames.com/graphql', {
            method: "post",
            body: JSON.stringify({
                operationName: "query",
                variables: {
                    accountIds: accountChunk,
                },
                query: accountQuery,
            }),
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(async res => {
                if (res.ok) {
                    return (await res.json()).data.Account.accounts
                }
                return
            })

    }))

    return accounts;
};

export { queryAccounts };