import { browser } from '$app/environment';
import { derived, writable } from 'svelte/store';

import type { ReplayFile } from '$lib/types';

type AnalyzedGames = Record<string, ReplayFile>

export const analyzedGames = writable<AnalyzedGames>(browser ? JSON.parse(localStorage.getItem("analyzedGames") ?? "{}") : {});

if (browser) {
    analyzedGames.subscribe(val => localStorage.analyzedGames = JSON.stringify(val))
}

export const recognizedPlayers = derived(analyzedGames, $analyzedGames => {
    const persons = Object.keys($analyzedGames).map(key => {
        return [...new Set($analyzedGames[key].eliminations.map((elim) => { return [elim.eliminated.id, elim.eliminator.id] }).flat())].filter(Boolean)
    }).flat()

    const count = persons.reduce((acc, val) => {
        if (!acc.get(val)) {
            acc.set(val, 0)
        }
        acc.set(val, acc.get(val) + 1)
        return acc
    }, new Map<string, number>())

    return count
}, new Map<string, number>())