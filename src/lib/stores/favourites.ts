import { browser } from '$app/environment';
import { writable } from 'svelte/store';

export const favourites = writable<string[]>(browser ? JSON.parse(localStorage.getItem("favourites") ?? "[]") : []);

if (browser) {
    favourites.subscribe(val => localStorage.favourites = JSON.stringify(val))
}